import Vue from 'vue'
import Vuex from 'vuex'

import SHIPMENT from '@/store/Shipment'

Vue.use(Vuex)

export default new Vuex.Store({
	strict: true,
	modules: {
		SHIPMENT
	},
	state: {},
	mutations: {},
	actions: {},
	getters: {}
})
