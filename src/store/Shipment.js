

const DEFAULT_STATE = () => ({
	shipments: [],
	data: null,
	keyword: null,
	updated: false,
	loading: false,
	modified: false,
})

const ACTIONS = {
	async requestShipmentLoad(ctx) {
		if (ctx.getters.shipmentsCount)
			return

		ctx.commit('setLoading', true)

		const res = await
			fetch('http://localhost:3010/shipments')
			.then(response => response.json())

		ctx.commit('clearShipment')
		ctx.commit('setShipments', res)
	},

	async requestShipmentData(ctx, { shipment_id }) {
		ctx.commit('setLoading', true)

		const res = await
			fetch(`http://localhost:3010/shipments/${shipment_id}`)
			.then(response => response.json())

		ctx.commit('setLoading', false)
		ctx.commit('setModified', false)
		ctx.commit('setShipmentData', res)
	},

	async requestShipmentUpdate(ctx) {
		ctx.commit('setLoading', true)
		const { data } = ctx.state

		await fetch(`http://localhost:3010/shipments/${data.id}`, {
			method: 'PUT',
			headers: {
			'Content-type': 'application/json; charset=UTF-8'
			},
			body: JSON.stringify(data)
		})
		.then(response => response.json())

		ctx.commit('setModified', false)
	},

	editCargoBox(ctx, payload) {
		if (ctx.state.data.id !== payload.id)
			return

		ctx.commit('setModified', true)
		ctx.commit('setShipmentData', {
			...ctx.state.data,
			...payload
		})
	}
}

const MUTATIONS = {
	setShipments(state, payload) {
		state.shipments = payload
	},

	setShipmentData(state, payload) {
		state.data = payload
	},

	setLoading(state, payload) {
		state.loading = payload
	},

	setModified(state, payload) {
		state.modified = payload
	},

	setKeyword(state, payload) {
		state.keyword = payload
	},

	clearShipment(state) {
		Object.assign(state, DEFAULT_STATE() )
	}
}

const GETTERS = {
	shipments: state => state.shipments,
	shipmentsCount: state => state.shipments.length,
	modified: state => state.modified,
	loading: state => state.loading,
	updated: state => state.updated,
	data: state => state.data,
	filteredShipment(state, getters) {
		if (!getters.shipmentsCount)
			return []

		if (state.keyword)
			return state.shipments
				.filter(({ name }) => name.toLowerCase().includes(state.keyword.toLowerCase()))

		return state.shipments
	}
}

const SHIPMENT = {
	namespaced: true,
  state: DEFAULT_STATE() ,
  actions:ACTIONS,
  mutations: MUTATIONS,
	getters: GETTERS
}

export default SHIPMENT