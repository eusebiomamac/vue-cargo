import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: { name: 'Shipment' }
  },
  {
    path: '/shipment/:shipment_id?',
    name: 'Shipment',
    component: () => import('@/container/Shipment.vue')
  },
]

const router = new VueRouter({
	mode: 'history',
  routes
})

export default router
